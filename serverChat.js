// 1.モジュールオブジェクトの初期化
var fs = require("fs");
var server = require("http").createServer();
var order = require("./order");
var now = new Date();
server.on('request', doRequest);
server.listen(3939);
// var server = require("http").createServer(function(req, res) {
//      res.writeHead(200, {"Content-Type":"text/html"});
//      var output = fs.readFileSync("./index.html", "utf-8");
//      res.end(output);
// }).listen(3939);

console.log("server:3939");

function doRequest(request, response) {
    switch (request.url) {
        case 'url':
            fs.readFile('./index.html', 'UTF-8',
                function(err, data) {
                    response.writeHead(200, {'Content-Type': 'text/html'});
                    response.write(data);
                    response.end();
                }
            );
            break;
        case '/style.css':
            fs.readFile('./style.css', 'UTF-8',
                function(err, data) {
                    response.writeHead(200, {'Content-Type': 'text/css'});
                    response.write(data);
                    response.end();
                }
            );
            break;
    }

};
var io = require("socket.io").listen(server);


// ユーザ管理ハッシュ
var userHash = {};

// 2.イベントの定義
io.sockets.on("connection", function (socket) {

  // 接続開始カスタムイベント(接続元ユーザを保存し、他ユーザへ通知)
  socket.on("connected", function (name) {
    var msg = name + "が入室しました";
    // userHash[socket.id] = name;
    var rooms = order.getRooms(name);
    for (var room of rooms){
      socket.join(room);
      io.to(room).emit("publish",{value:msg});
    }

  });

  //orderとその下をつくる
  //clientから送られてきたメッセージをその部屋の人たちに贈る。
  //errorが起きた場合はpublishStampのえらーとわかるようにmessageをいれて送る
  //引数　json parseしてobjectに変換してそののちに同名のfunctionに渡す。
  socket.on("publishStamp",function(json) {
    var data = JSON.parse(json);
    publishStamp(data);
  });
  //publishStamp
  //引数　object  戻り値　ルームに対して送信
  function publishStamp(data){
    try{
    var obj = order.addEventStamp(data,socket.id);
    var json = JSON.stringify(obj);
    io.to(data.roomID).emit("publishStamp",json);
    }catch(e){
      error(e);
    }
  }////

  socket.on("publishMessage",function(json){
    var data = JSON.parse(json);
    publishMessage(data);
  });
  //publishMessage roomにいる人達に送信するためのメソッド
  //引数　object
  function publishMessage(data){
    try{
      var obj = order.addEventMessage(data,socket.id);
      var json = JSON.stringify(obj);
      io.to(roomID).emit("publishMessage",json);
    }catch(e){
      error(e);
    }
  }//
//
//
  socket.on("getStamp",function(json){//完成
    var data = JSON.parse(json);
    getStamp(data);
  });
  //自分のstampID一覧を取得する
  function getStamp(data){
    try{
      var ary = order.getStamps(data);
      var json = JSON.stringify(ary);
      io.to(socket.id).emit("getStamp",json);
    }catch(e){
      error(e);
    }
  }//

  socket.on("getMessages",function(json){//完成
    var data = JSON.parse(json);
    getMessages(data);
  });
  function getMessages(data){
    try{
      var ary = order.getMessage(data);
      var json = JSON.stringify({messages:ary});
      io.to(socket.id).emit("getMessage",json);
    }catch(e){
      error(e);
    }
  }
//whiteGoat
//引数　json
//戻り値なし
  socket.on("whiteGoat",function(data){
    var value = JSON.parse(data);
    whiteGoat(data);
  });
//whiteGoat
//引数　object
// 送信値　object {roomID,userID,timeStamp}
    function whiteGoat(data){
      try{
        var judge = order.whiteGoat(data);
        var json = JSON.stringify(data);
        io.to(socket.id).emit("whiteGoat",json);
      }catch(e){
        error(e);
      }
  }
//konoyarikata
  // socket.on("enterRoom",function(json){
  //   var data = JSON.parse(json);
  //   enterRoom(data);
  // });
  // function enterRoom(data){
  //   while(true){
  //     var name = order.enterRoom(data.userID,data.roomID);
  //     if(!name.equals("")){break;}
  //   }
  //   socket.join(data.roomID);
  //   var json = JSON.stringify({roomID:roomID,userID:userID});
  //   io.to(roomID).emit("enterRoom",json);
  // }
  // socket.on("leaveRoom",function(json){
  //   var data = JSON.parse(json);
  //   leaveRoom(data);
  // });
  // function leaveRoom(data){
  //   order.leaveRoom(data.userID,data.roomID);
  //   io.to(roomID).emit("leaveRoom",JSON.stringify(data));
  // }
  //未完成機能
  socket.on("getParticipants",function(json){//完成
    var data = JSON.parse(json);
    getParticipants(data);
  });
  //getParticipants
  //引数　object
  //送信　getParticipants
  //戻り値梨
  function getParticipants(data){
    try{
      var obj = order.getParticipants(data);
      var json = JSON.stringify(obj);
      io.to(socket.id).emit("getParticipants",json);
    }catch(e){
      error(e);
    }
  }


  function error(mssage){
    var json = JSON.stringify({msg:message});
    io.to(socket.id).emit("error",json);
  }


////メッセージ送信イベント　
//// 接続終了組み込みイベント(接続元ユーザを削除し、他ユーザへ通知)
	   socket.on("disconnect", function () {
    if (userHash[socket.id]) {
      var msg = userHash[socket.id] + "が退出しました";
      delete userHash[socket.id];
      io.sockets.emit("publish", {value: msg});
    }
  });
});
