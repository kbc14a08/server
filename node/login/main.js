/*
これ単体でテストするときは、
getUrlの埋め込んでいるurlのポート番号を変えればいい。
*/
//moduleのロード
var http = require('http');
var crypto = require('crypto');

//そのうちURLの取得の処理を実装（現在仮）
var url = getUrl();
var key = 'wordpress_logged_in_'+ hash('hex', url);

//サーバーのresponse,requestの保持(必要？)
var req,res;

//ユーザ情報を入れる
var user = null;



//メインの処理
//ログイン済みかの判定
function wpValidate(req,res,callback){
	setReq_Res(req,res);

	//cookieに何かしら埋め込まれているとき
	if(req.headers.cookie){
		var value = getCookie(req.headers.cookie, key);

		//http.requestのオプション
		//path,portを変える必要あり
		var op = {
			hostname: 'localhost',
			path:'/wp-content/plugins/web-line/login_test.php?c='+encodeURI(value),
			port: 8080,
			method: 'GET'
		};

		//phpファイルにhttp.requestでアクセス
		var hreq = http.request(op,hreq_in);

		//通信の失敗時
		hreq.on('error', error);

		//終了
		hreq.end();
	}else{
		//cookieがない場合の処理
		//res.send('cookie:undefined');
	}
	callback();
}


//必要性的にはどうなのか？
function setReq_Res(request,response){
	req = request;
	res = response;
}

//TODO　関数名要変更 a
//http.requestにぶち込むやつ
function hreq_in(response){
	//仮処理
	console.log('status:'+response.statusCode);
	response.on('data',set);
	console.log('success');
}


//TODO　関数名要変更 b
//a(仮)にぶち込むやつ
function set(chunk){
	//仮
	console.log('body:'+chunk.toString());

	//成功時user情報を入れる
	//userの内容は未定
	//判定はもっとなんかいい感じでできるのでは？
	if(chunk.toString() == 'true'){
		user = 'true';
	}else{
		user = null;
	}

	console.log(user);
}


//通信とかなんかのエラー時処理
function error(e){
	//仮
	console.log('problem with request: ' + e.message);
}


//str:ハッシュ化したい文字列　 url:wordpressのURL
//戻り値:md5でハッシュ化した結果
function hash(str,url){
	var md5 = crypto.createHash('md5');
	md5.update(url);

	return md5.digest(str);
}


//必要なcookieを取得
function getCookie(cookie,key){
	var value = '';
	var cookie_str = cookie.split('; ');

	//必要なcookie情報を検索
	for(var i = 0; i < cookie_str.length; i++){
		//keyと一致した場合にkeyを空文字で置き換える
		if(cookie_str[i].indexOf(key) != -1){
			value = cookie_str[i].replace(key+'=','');

			return value;
		}
	}
	return '';
}


//wordpressからURLを取得したい
function getUrl() {
	//仮のデータを返す。
	var url = 'http://localhost:8080';
	return url;
}

function getUser(){
	return user;
}
//モジュール化
module.exports.wpValidate = wpValidate;
