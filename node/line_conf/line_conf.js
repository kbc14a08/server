module.exports = {        
        //設定テーブル作成
        //作成に成功すればtrue
        //失敗すればfalse
        initConf : function(callback) {
                //DB情報読み込み
                var db_conf = require('./mysql-conf');
        
                //mysqlモジュール読み込み 
                var mysql = require('mysql');
                
                //トランザクション用のマスターの接続情報をセット
                //host: ホスト情報
                //user: DBのユーザーID
                //password: 接続用パスワード
                //database:　DB名
                //port: ポート番号
                var mysql_options = {
                        host: db_conf.HOST,
                        user: db_conf.MYSQL_USER,
                        password: db_conf.MYSQL_PASS,
                        database: db_conf.DATABASE,
                        port: db_conf.PORT
                };
                
                //データーベースへ接続
                var my_client = mysql.createConnection(mysql_options);
                my_client.connect();
        
                var sql = "CREATE TABLE line_config (conf_key varchar(10) PRIMARY KEY, conf_value varchar(10))";
            
                my_client.query(sql, function(error, results, fields) {
                        //エラー処理
                        if (error) {
                                //console.log(error);
                                callback(false);
                                return;
                        }
                        //正常に処理が完了した場合の処理
                        if (results) {
                                callback(true);
                        }
                });
                my_client.end();
        },
        
        //keyに対応するvalueを返す
        //未登録・エラーの場合はnullを返す
        getConf : function(key, callback) {
                //DB情報読み込み
                var db_conf = require('./mysql-conf');

                //mysqlモジュール読み込み 
                var mysql = require('mysql');
                
                //トランザクション用のマスターの接続情報をセット
                //host: ホスト情報
                //user: DBのユーザーID
                //password: 接続用パスワード
                //database:　DB名
                //port: ポート番号
                var mysql_options = {
                        host: db_conf.HOST,
                        user: db_conf.MYSQL_USER,
                        password: db_conf.MYSQL_PASS,
                        database: db_conf.DATABASE,
                        port: db_conf.PORT
                };
                
                //データーベースへ接続
                var my_client = mysql.createConnection(mysql_options);
                my_client.connect();
                var sql = "select conf_value FROM line_config WHERE conf_key =" + key;
                
                my_client.query(sql, function(error, results, fields) {
                        //エラー処理
                        if (error) {
                                //console.log(error);
                                callback(null);
                                return;
                        }
                        
                        //クエリが通った時の処理
                        if (results) {
                                //正常に値が取得できれば値を返す
                                for(var i in results) {
                                        var value = results[i].conf_value;
                                        callback(value);
                                        return;
                                }
                                
                                //取得できていなければnull
                                callback(null);
                        }
                });
                my_client.end();
        },
        
        //keyに対応するvalueを上書きする
        //成功すればtrue
        //失敗すればfalse
        setConf : function (key, value, callback) {
                //DB情報読み込み
                var db_conf = require('./mysql-conf');
                
                //mysqlモジュール読み込み 
                var mysql = require('mysql');
                
                //トランザクション用のマスターの接続情報をセット
                //host: ホスト情報
                //user: DBのユーザーID
                //password: 接続用パスワード
                //database:　DB名
                //port: ポート番号
                var mysql_options = {
                        host: db_conf.HOST,
                        user: db_conf.MYSQL_USER,
                        password: db_conf.MYSQL_PASS,
                        database: db_conf.DATABASE,
                        port: db_conf.PORT
                };
                
                //データーベースへ接続
                var my_client = mysql.createConnection(mysql_options);
                my_client.connect();
                
                var sql = "update line_config set conf_value = " + value + " where conf_key =" + key;
                
                my_client.query(sql, function(error, results, fields) {
                        //エラー処理
                        if (error) {
                                //console.log(error);
                                callback(false);
                        }
                        
                        //クエリが通った時の処理
                        if (results) {
                                //更新できていればtrue
                                //できていなければfalse
                                if(results.affectedRows == 1) {
                                        callback(true);
                                } else {
                                        callback(false);                                
                                }
                        }
                });
                my_client.end();
        }
};